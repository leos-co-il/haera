<?php
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 22 );
add_filter( 'woocommerce_single_product_summary', 'woo_template_single_text', 15 );
function woo_template_single_text($content){
	echo '<div class="base-output slider-output mb-4 mt-3">'.the_content().'</div>';
}
add_action('woo_custom_breadcrumb', 'woocommerce_breadcrumb');
add_filter( 'woocommerce_breadcrumb_defaults', 'wps_breadcrumb_delimiter' );
//add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 6, 2 );
function wpa83367_price_html( $price, $product ){
	return str_replace( '<ins>', '<div>', $price );
}
function wps_breadcrumb_delimiter( $defaults ) {
	$defaults['delimiter'] = ' >> ';
	return $defaults;
}
add_filter('woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1);
add_action( 'woocommerce_before_add_to_cart_quantity', 'ls_display_quantity_plus' );
function iconic_cart_count_fragments($fragments){
	$fragments['div.header-cart-count'] = '<div class="centered header-cart-count">' . WC()->cart->get_cart_contents_count() . '</div>';
	return $fragments;
}
function ls_display_quantity_plus() {
	echo '<button type="button" class="plus">+</button>';
}
add_action( 'woocommerce_after_add_to_cart_quantity', 'ls_display_quantity_minus' );
function ls_display_quantity_minus() {
	echo '<button type="button" class="minus">-</button>';
}
//function move_variation_price() {
//	remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
//	add_action( 'woocommerce_after_single_variation', 'woocommerce_single_variation', 10 );
//}
//add_action( 'woocommerce_before_add_to_cart_form', 'move_variation_price' );
/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 6 );

function new_loop_shop_per_page( $cols ) {
	// $cols contains the current number of products per page based on the value stored on Options –> Reading
	// Return the number of products you wanna show per page.
	$cols = 6;
	return $cols;
}
?>

