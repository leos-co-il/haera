<?php

function text_preview($phrase, $max_words) {
    $phrase = strip_shortcodes($phrase);
    $phrase = strip_tags($phrase);
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

function postThumb($post = false, $size = 'full', $placeholder_size = 'regular', $placeholder = true){
    $postThumb = IMG . 'placeholder-'. $placeholder_size .'.png';
    if(ENV === 'dev' || ENV === 'qa'){
        $postThumb = 'https://picsum.photos/60' . mt_rand(1,9);
    }
    if(!$post){
        $post = get_post();
    }
    if($post && get_the_post_thumbnail_url($post)){
        $postThumb = get_the_post_thumbnail_url($post, $size);
    }

    if($placeholder){
        return $postThumb;
    }else{
        return false;
    }

}
function getYoutubeId($url){
    parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
    return $my_array_of_vars['v'];
}
function getYoutubeThumb($url){
    return "https://img.youtube.com/vi/".getYoutubeId($url)."/0.jpg";
}
function getForm($id){
    echo do_shortcode('[contact-form-7 id="'.$id.'"  html_class="use-floating-validation-tip"]');
}
function svg_simple($path){
    return file_get_contents($path);
}
function svg_ext($url, $class='',$field = '',$alt = '') {
    $nosvg =  ICONS . 'nosvg.png';
    if ($field == 'field') {
        $url = get_field($url);
    } elseif ($field == 'options') {
        $url = get_field($url,'options');
    }
    if (is_array($url)) {
        if (empty($alt)) { $alt = $url[alt]; }
        $url = $url[url];
        $svg = $url;
    } else {
        $svg = $url;
    }
    $svg = explode('.', $svg);
    $csvg = count($svg);
    if ($csvg == 1) {
        $path = TEMPLATEPATH.'/images/'.$svg[0].'.svg';
        if (file_exists($path)) {
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$nosvg.'" alt="" class="'.$class.'"/>';
        }
    } else {
        $place = $csvg - 1;
        if ($svg[$place] == 'svg') {
            $url = explode('/',$url);
            $c = count($url) - 1;
            $path = WP_CONTENT_DIR.'/uploads/'.$url[$c - 2].'/'.$url[$c - 1].'/'.$url[$c];
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$url.'" alt="'.$alt.'" class="'.$class.'"/>';
        }
    }
    return $svg;
}
function getMenu($location, $depth, $menu_class = '', $container_class = ''){
    wp_nav_menu([
        'theme_location' => $location,
        'container'       => 'div',
        'container_id'    => '',
        'container_class' => $container_class,
        'menu_id'         => false,
        'menu_class'      => $menu_class,
        'depth'           => $depth,
    ]);
}
function opt($field_name){
    if(function_exists('get_field')){
        return get_field($field_name, 'options');
    }
    return null;
}
function getPhone(){
    $ph = opt('tel');
    $phone = [];
    if ($ph) {
        $phone["txt"] = $ph;
        $phone["num"] = preg_replace("/[^A-Za-z0-9]/", "",$ph);
        $phone["tel"] = 'tel:'.$phone["num"];
        $phone["link"] = '<a href="tel:'.$phone["num"].'">'.$ph.'</a>';
    }

    return $phone;
}

function getAddress(){
    $address = opt('address');
    if ($address) {
        $addr = explode(',',$address["address"]);
        $address["name"] = $addr[0].', '.$addr[1];
        $address["gmlink"] = 'http://www.google.com/maps/place/'.$address["lat"].','.$address["lng"];
        $address["waze"] = 'waze://?q='.$address["name"];
    }

    return $address;
}
function include_part($name, $once = false){
    locate_template('partials/'.$name.'.php', true, $once);
}
function getPageByTemplate($template){
	$args = [
		'post_type' => 'page',
		'fields' => 'ids',
		'nopaging' => true,
		'meta_key' => '_wp_page_template',
		'meta_value' => $template
	];
	$pages = get_posts( $args );
	return $pages[0];
}
function get_lang(){
	if ( function_exists('icl_object_id') ) {
		return ICL_LANGUAGE_CODE;
	}
	return null;
}
function lang_text ($text_arr, $default_lang) {
	if ( function_exists('icl_object_id') ) {
		$lang = ICL_LANGUAGE_CODE;
		return $text_arr[$lang];
	}
	return $text_arr[$default_lang];
}
function lang_form ($arr_id, $default_id) {
	if ( function_exists('icl_object_id') ) {
		$lang = ICL_LANGUAGE_CODE;
		$id = $arr_id[$lang];
		return getForm($id);
	}
	return getForm($default_id);
}
function site_languages() {
	if ( function_exists( 'icl_get_languages' ) ) :
		$languages = icl_get_languages( 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
		if ( ! empty( $languages ) ) :
			echo "\n<ul class=\"languages\">\n";
			foreach ( $languages as $lang ) :
				echo '<li class="' . ( $lang['active'] ? 'active' : ''  ) . '"><a href=' . $lang['url'] . '>' . $lang['language_code'] . "</a></li>\n";
			endforeach;
			echo "</ul>\n";
		endif; // ( ! empty( $languages ) )
	endif; // ( function exists )
}
function get_social_links ($names) {
	$socials = [];
	foreach ($names as $name) {
		if (opt($name)) {
			$socials[] = ['url' => opt($name), 'name' => $name];
		}
	}
	return $socials;
}
function load_template_part($template_name, $part_name = null, $params) {
	ob_start();
	get_template_part($template_name, $part_name, $params);
	$var = ob_get_contents();
	ob_end_clean();
	return $var;
}

