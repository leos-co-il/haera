<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$post_type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : '';
	$tax_name = (isset($_REQUEST['taxType'])) ? $_REQUEST['taxType'] : 'category';
	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $post_type ? $post_type : 'post',
		'posts_per_page' => 3,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => $tax_name,
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	if (($post_type === 'post') || ($post_type === 'service')) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	} elseif ($post_type === 'vacancy') {
		foreach ($query->posts as $x => $item) {
			$html = load_template_part('views/partials/card', 'vacancy_ajax', [
				'item' => $item,
				'number' => $x,
			]);
			$result['html'] .= $html;
		}
	}
//	elseif ($post_type === 'product') {
//		foreach ($query->posts as $item) {
//			$html = load_template_part('views/partials/card', 'product', [
//				'product' => $item,
//			]);
//			$result['html'] .= $html;
//		}
//	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
function add_product_to_cart() {
	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$type = isset($_REQUEST['type']) ? sanitize_text_field($_REQUEST['type']) : null;
	$quantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );

	$cart = WC()->cart;
	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}
	$result['type'] = "success";
	die();
}

add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');


add_action( 'wp_ajax_loadmore', 'loadmore' );
add_action( 'wp_ajax_nopriv_loadmore', 'loadmore' );

function loadmore() {

	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$ids = explode(',', $ids_string);
	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$paged++;

	$args = array(
		'paged' => $paged,
		'post_type' => 'product',
		'post_status' => 'publish',
		'post__not_in' => $ids,
	);
	$query = new WP_Query( $args );
	$html = '';
	$result['html'] = '';
	foreach( $query->posts as $item) :
		$html = load_template_part('views/partials/card', 'product', [
			'product' => $item,
		]);
		$result['html'] .= $html;
	endforeach;
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();

}
