<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות מוצרים',
            'singular_name'              => 'קטגוריות מוצרים',
            'menu_name'                  => 'קטגוריות מוצרים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}
if (SERVICES) {
	function service_post_type()
	{

		$labels = array(
			'name' => 'שירותים',
			'singular_name' => 'שירות',
			'menu_name' => 'שירותים',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל השירותים',
			'view_item' => 'הצג שירות',
			'add_new_item' => 'הוסף שירות חדש',
			'add_new' => 'הוסף חדש',
			'edit_item' => 'ערוך שירות',
			'update_item' => 'עדכון שירות',
			'search_items' => 'חפש שירות',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'service',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'service',
			'description' => 'שירותים',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('service_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-buddicons-topics',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('service', $args);

	}

	add_action('init', 'service_post_type', 0);

	function service_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות שירותים',
			'singular_name' => 'קטגוריות שירותים',
			'menu_name' => 'קטגוריות שירותים',
			'all_items' => 'כל השירותים',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'service_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('service_cat', array('service'), $args);

	}

	add_action('init', 'service_taxonomy', 0);
}
if(COURSES){
	function course_post_type() {

		$labels = array(
			'name'                => 'קורסים',
			'singular_name'       => 'קורס',
			'menu_name'           => 'קורסים',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל הקורסים',
			'view_item'           => 'הצג קורס',
			'add_new_item'        => 'הוסף קורס',
			'add_new'             => 'הוסף קורס חדש',
			'edit_item'           => 'ערוך קורס',
			'update_item'         => 'עדכון קורס',
			'search_items'        => 'חפש קורס',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'course',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'course',
			'description'         => 'קורסים',
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies'          => array( 'course_cat' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-format-aside',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'course', $args );

	}

	add_action( 'init', 'course_post_type', 0 );

	function course_taxonomy() {

		$labels = array(
			'name' => 'קטגוריות קורסים',
			'singular_name' => 'קטגוריות קורסים',
			'menu_name' => 'קטגוריות קורסים',
			'all_items' => 'כל הקורסים',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'type',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'course_cat', array( 'course' ), $args );

	}

	add_action( 'init', 'course_taxonomy', 0 );
}
if(CLASSES){
	function class_post_type() {

		$labels = array(
			'name'                => 'חוגים',
			'singular_name'       => 'חוג',
			'menu_name'           => 'חוגים',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל החוגים',
			'view_item'           => 'הצג חוג',
			'add_new_item'        => 'הוסף חוג',
			'add_new'             => 'הוסף חוג חדש',
			'edit_item'           => 'ערוך חוג',
			'update_item'         => 'עדכון חוג',
			'search_items'        => 'חפש פריטים',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'class',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'class',
			'description'         => 'חוגים',
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies'          => array( 'class_cat' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-universal-access',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'class', $args );

	}

	add_action( 'init', 'class_post_type', 0 );

	function class_taxonomy() {

		$labels = array(
			'name'                       => 'קטגוריות חוגים',
			'singular_name'              => 'קטגוריות חוגים',
			'menu_name'                  => 'קטגוריות חוגים',
			'all_items'                  => 'כל הקטגוריות',
			'parent_item'                => 'קטגורית הורה',
			'parent_item_colon'          => 'קטגורית הורה:',
			'new_item_name'              => 'שם קטגוריה חדשה',
			'add_new_item'               => 'להוסיף קטגוריה חדשה',
			'edit_item'                  => 'ערוך קטגוריה',
			'update_item'                => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items'               => 'חיפוש קטגוריות',
			'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'class_cat',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'class_cat', array( 'class' ), $args );

	}

	add_action( 'init', 'class_taxonomy', 0 );
}

if (CAMPS) {
	function camp_post_type()
	{

		$labels = array(
			'name' => 'קייטנות',
			'singular_name' => 'קייטנה',
			'menu_name' => 'קייטנות',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל הקייטנות',
			'view_item' => 'הצג שירות',
			'add_new_item' => 'הוסף קייטנה חדש',
			'add_new' => 'הוסף חדש',
			'edit_item' => 'ערוך קייטנה',
			'update_item' => 'עדכון קייטנה',
			'search_items' => 'חפש קייטנה',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'camp',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'camp',
			'description' => 'קייטנות',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('camp_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-admin-multisite',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('camp', $args);

	}

	add_action('init', 'camp_post_type', 0);

	function camp_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות קייטנות',
			'singular_name' => 'קטגוריות קייטנות',
			'menu_name' => 'קטגוריות קייטנות',
			'all_items' => 'כל הקייטנות',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'camp_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('camp_cat', array('camp'), $args);

	}

	add_action('init', 'camp_taxonomy', 0);
}
if (VACANCIES) {
	function vacancy_post_type()
	{
		$labels = array(
			'name' => 'משרות',
			'singular_name' => 'משרה',
			'menu_name' => 'משרות',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל המשרות',
			'view_item' => 'הצג משרה',
			'add_new_item' => 'הוסף משרה חדשה',
			'add_new' => 'הוסף חדשה',
			'edit_item' => 'ערוך משרה',
			'update_item' => 'עדכון משרה',
			'search_items' => 'חפש משרה',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'vacancy',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'vacancy',
			'description' => 'משרה',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('position', 'region', 'vacancy_format'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-id-alt',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('vacancy', $args);

	}

	add_action('init', 'vacancy_post_type', 0);

	function position_taxonomy() {
		$labels = array(
			'name' => 'תפקידים',
			'singular_name' => 'תפקיד',
			'menu_name' => 'תפקידים',
			'all_items' => 'כל התפקידים',
			'parent_item' => 'תפקידים הורה',
			'parent_item_colon' => 'תפקידים הורה:',
			'new_item_name' => 'שם תפקיד חדש',
			'add_new_item' => 'להוסיף תפקיד חדש',
			'edit_item' => 'ערוך תפקיד',
			'update_item' => 'עדכן תפקיד',
			'separate_items_with_commas' => 'תפקידים נפרדות עם פסיק',
			'search_items' => 'חיפוש תפקידים',
			'add_or_remove_items' => 'להוסיף או להסיר תפקידים',
			'choose_from_most_used' => 'בחר מהתפקידים הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'position',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('position', array('vacancy'), $args);
	}
	add_action('init', 'position_taxonomy', 0);

	function region_taxonomy() {
		$labels = array(
			'name' => 'מיקומים',
			'singular_name' => 'מיקום',
			'menu_name' => 'מיקומים',
			'all_items' => 'כל המיקומים',
			'parent_item' => 'מיקומים הורה',
			'parent_item_colon' => 'מיקומים הורה:',
			'new_item_name' => 'שם מיקום חדש',
			'add_new_item' => 'להוסיף מיקום חדש',
			'edit_item' => 'ערוך מיקום',
			'update_item' => 'עדכן מיקום',
			'separate_items_with_commas' => 'מיקומים נפרדות עם פסיק',
			'search_items' => 'חיפוש מיקומים',
			'add_or_remove_items' => 'להוסיף או להסיר מיקומים',
			'choose_from_most_used' => 'בחר מהמיקומים הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'region',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('region', array('vacancy'), $args);
	}
	add_action('init', 'region_taxonomy', 0);

	function vacancy_format_taxonomy() {
		$labels = array(
			'name' => 'היקף משרה',
			'singular_name' => 'היקף משרה',
			'menu_name' => 'היקפי משרות',
			'all_items' => 'כל היקפי משרות',
			'parent_item' => 'היקפי משרות הורה',
			'parent_item_colon' => 'היקפי משרות הורה:',
			'new_item_name' => 'שם היקף משרה חדש',
			'add_new_item' => 'להוסיף היקף משרה חדש',
			'edit_item' => 'ערוך היקף משרה',
			'update_item' => 'עדכן היקף משרה',
			'separate_items_with_commas' => 'היקפי משרות נפרדות עם פסיק',
			'search_items' => 'חיפוש היקפי משרות',
			'add_or_remove_items' => 'להוסיף או להסיר היקפי משרות',
			'choose_from_most_used' => 'בחר מההיקפי משרות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'vacancy_format',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('vacancy_format', array('vacancy'), $args);
	}
	add_action('init', 'vacancy_format_taxonomy', 0);
}
