<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'course_cat', ['fields' => 'ids']);

?>

<article class="page-body mt-4 mb-5 sillabus-body">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-md-11 col-12">
				<div class="row justify-content-center">
					<?php if ( function_exists('yoast_breadcrumb') ) : ?>
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['sil_cat_course_links'] || $fields['sil_cat_class_links']) : ?>
					<div class="row justify-content-center align-items-stretch mb-4">
						<?php if ($fields['sil_cat_course_links']) : foreach ($fields['sil_cat_course_links'] as $link) : ?>
							<div class="col-auto sil-col">
								<a href="<?= get_term_link($link); ?>" class="sil-link <?= (in_array($post_terms, $link->term_id))
										? 'curr-sil-link-item' : ''; ?>">
									<?= $link->name; ?>
								</a>
							</div>
						<?php endforeach; endif;
						if ($fields['sil_cat_class_links']) : foreach ($fields['sil_cat_class_links'] as $link) :?>
							<div class="col-auto sil-col">
								<a href="<?= get_term_link($link); ?>" class="sil-link
								<?= (in_array($post_terms, $link->term_id)) ? 'curr-sil-link-item' : ''; ?>">
									<?= $link->name; ?>
								</a>
							</div>
						<?php endforeach; endif; ?>
					</div>
				<?php endif;
				if ($fields['sil_post_links']) : ?>
					<div class="row justify-content-center align-items-stretch mb-4">
						<?php foreach ($fields['sil_post_links'] as $post_link) : ?>
							<div class="col-auto sil-col sil-post-col">
								<a href="<?php the_permalink($post_link); ?>" class="sil-post-link
								<?= ($post_link->ID === get_the_ID()) ? 'curr-sil-post-link' : ''; ?>">
									<?= $post_link->post_title; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if ($fields['sil_table']) : ?>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
							<tr>
								<th scope="col">
									<?= lang_text(['he' => 'מס’ שיעור', 'en' => 'Lesson number'], 'he'); ?>
								</th>
								<th scope="col">
									<?= lang_text(['he' => 'דיסציפילינה מדעית', 'en' => 'Scientific discipline'], 'he'); ?>
								</th>
								<th scope="col">
									<?= lang_text(['he' => 'מנת למידה', 'en' => 'Learning dose'], 'he'); ?>
								</th>
								<th scope="col">
									<?= lang_text(['he' => 'שם הפרויקט', 'en' => 'Project name'], 'he'); ?>
								</th>
								<th scope="col">
									<?= lang_text(['he' => 'תמונות מהפרויקט', 'en' => 'Photos from the project'], 'he'); ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($fields['sil_table'] as $x => $row) : ?>
								<tr>
									<td class="number-col"><?= $x + 1; ?></td>
									<td class="name-col">
										<?= $row['row_name']; ?>
									</td>
									<td>
										<p class="base-text-row">
											<?= $row['row_dose']; ?>
										</p>
									</td>
									<td>
										<?php if ($row['row_name_project']): foreach ($row['row_name_project'] as $pr) : ?>
											<div class="row-pr-item">
												<p class="base-text-row"><?= $pr['pr_text']; ?></p>
												<?php if ($pr['pr_link']) : ?>
													<a href="<?= $pr['pr_link']['url']; ?>" class="row-pr-link">
														<?= $pr['pr_link']['title'] ? $pr['pr_link']['title'] :
																lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
													</a>
												<?php endif; ?>
											</div>
										<?php endforeach; endif; ?>
									</td>
									<td>
										<?php if ($row['row_img']) : ?>
											<img src="<?= $row['row_img']['url']; ?>" alt="project-img">
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
