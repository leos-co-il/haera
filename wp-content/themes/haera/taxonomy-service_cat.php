<?php
get_header();
$query = get_queried_object();
$posts = get_posts([
	'numberposts' => 8,
	'post_type' => 'service',
	'suppress_filters' => false
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'service',
	'suppress_filters' => false
]);
$cats = get_terms([
	'taxonomy' => 'service_cat',
	'hide_empty' => false,
]);
?>
<article class="page-body">
	<div class="body-output">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-11 col-12">
					<div class="row">
						<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
					<div class="row justify-content-center mt-3">
						<div class="col">
							<h1 class="block-title"><?= $query->name; ?></h1>
						</div>
					</div>
					<?php if ($posts) : ?>
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($posts as $post) {
								get_template_part('views/partials/card', 'post', [
									'post' => $post,
								]);
							} ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (count($posts_all) > 8) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="service"
						 data-tax-type="service_cat">
						<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<div class="form-without-margins">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($slider = get_field('single_slider_seo', $query)) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $slider,
			'img' => get_field('slider_img', $query),
		]); ?>
	</div>
<?php endif;
if ($all_faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'block_desc' => get_field('faq_text', $query),
			'faq' => $all_faq,
		]);
}
get_footer(); ?>

