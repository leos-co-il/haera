<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body mt-4 mb-5">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-md-11 col-12">
				<div class="row justify-content-center">
					<?php if ( function_exists('yoast_breadcrumb') ) : ?>
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-between align-items-start">
					<div class="col-lg-6 col-12 post-content-col">
						<h1 class="base-title-beige"><?php the_title(); ?></h1>
						<div class="base-output base-single-output">
							<?php the_content(); ?>
						</div>
						<div class="socials-share">
					<span class="share-text">
						<?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
							<!--	WHATSAPP-->
							<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
								<img src="<?= ICONS ?>whatsapp-share.png">
							</a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
							   class="social-share-link">
								<img src="<?= ICONS ?>facebook-share.png">
							</a>
							<!--	MAIL-->
							<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
							   class="social-share-link">
								<img src="<?= ICONS ?>mail-share.png">
							</a>
						</div>
					</div>
					<div class="col-xl-5 col-lg-6 col-12">
						<div class="pop-form-col single-form-col">
							<?php if ($logo_pop = opt('logo')) : ?>
								<a href="/" class="logo">
									<img src="<?= $logo_pop['url'] ?>" alt="logo">
								</a>
							<?php endif;
							if ($f_title = opt('post_form_title')) : ?>
								<h2 class="pop-form-title"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_subtitle = opt('post_form_subtitle')) : ?>
								<h3 class="pop-form-subtitle"><?= $f_subtitle; ?></h3>
							<?php endif;
							if ($f_text = opt('pop_form_text')) : ?>
								<p class="base-text mb-3"><?= $f_text; ?></p>
							<?php endif;
							lang_form(['he' => '12', 'en' => '92'], '12') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
$text = '';
switch ($currentType) {
	case 'post':
		$taxname = 'category';
		break;
	case 'class':
		$taxname = 'service_cat';
		break;
	default:
		$currentType = 'post';
		$taxname = 'category';
}
$type = $currentType === 'class';
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => $currentType,
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => $taxname,
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => $currentType,
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-slider arrows-slider">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-11">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($samePosts as $post) {
							get_template_part('views/partials/card', 'post',
									[
											'post' => $post,
									]);
						}?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_desc' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
}
get_footer(); ?>
