<?php

the_post();
get_header();
?>
<article class="page-body mt-5">
	<?php the_content(); ?>
</article>
<?php get_footer(); ?>
