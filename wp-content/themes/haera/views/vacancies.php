<?php
/*
Template Name: דרושים
*/

get_header();
$fields = get_fields();

$args = [];
$args['post_type'] = 'vacancy';
$args['suppress_filters'] = false;

$args['posts_per_page'] = 4;
$results = new WP_Query($args);
?>
<article class="page-body mt-3">
	<div class="container-fluid">
		<div class="row justify-content-center mb-4">
			<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
			<div class="col">
				<h2 class="block-title"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
	<?php if ($results->have_posts()) : ?>
		<div class="container-fluid mb-3">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-md-10 col-12">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($results->posts as $x => $post) : ?>
							<?php get_template_part('views/partials/card', 'vacancy_ajax', [
									'item' => $post,
									'number' => $x,
							]); ?>
						<?php endforeach; ?>
					</div>
					<div class="take-form">
						<div class="form-for-hire">
							<div class="form-vacancy-wrap">
								<?php if ($title_form = opt('vac_form_title')) : ?>
									<h2 class="show-title-form"><?= $title_form; ?></h2>
								<?php endif; ?>
								<?php if ($subtitle_form = opt('vac_form_subtitle')) : ?>
									<h3 class="show-subtitle-form"><?= $subtitle_form; ?></h3>
								<?php endif; ?>
								<?php lang_form(['he' => '17', 'en' => '95'], 'he'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="vacas-more">
		<div class="container-fluid mb-3">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-md-10 col-12">
					<div class="row justify-content-center align-items-stretch vacas">
					</div>
					<div class="take-form">
						<div class="form-for-hire">
							<div class="form-vacancy-wrap">
								<?php if ($title_form = opt('vac_form_title')) : ?>
									<h2 class="show-title-form"><?= $title_form; ?></h2>
								<?php endif; ?>
								<?php if ($subtitle_form = opt('vac_form_subtitle')) : ?>
									<h3 class="show-subtitle-form"><?= $subtitle_form; ?></h3>
								<?php endif; ?>
								<?php lang_form(['he' => '17', 'en' => '95'], 'he'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($results->have_posts()) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="vacancy"
						 data-tax-type="position">
						<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form'); ?>
<div class="mt-5">
	<?php
	if ($samePosts = $fields['misrot_pop']) :
		get_template_part('views/partials/content', 'slider_vacancy',
			[
				'content' => $samePosts,
				'title' => $fields['misrot_title'] ? $fields['misrot_title'] : 'משרות נוספות באותו תחום',
			]);
	endif; ?>
</div>
<!--Slider-->
<?php if ($slider_seo = $fields['single_slider_seo']) :
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $slider_seo,
			'img' => $fields['slider_img'],
		]);
endif;
if ($all_faq = $fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_desc' => $fields['faq_text'],
			'faq' => $all_faq,
		]);
}
get_footer(); ?>
