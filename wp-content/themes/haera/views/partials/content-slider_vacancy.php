<?php if (isset($args['content']) && $args['content']) : $vacs = $args['content']; ?>
	<div class="vacancies-output slider-with-vacancies">
		<div class="container-fluid">
			<?php if (isset($args['title']) && $args['title']) : $vac_block_title = $args['title']; ?>
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="base-title text-center mb-4"><?= $vac_block_title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center mb-5">
				<div class="col-11 another-col-width">
					<div class="slider-vacancies">
						<?php foreach ($vacs as $x => $slider_vacancy) : ?>
							<?php get_template_part('views/partials/card', 'vacancy',
								[
									'item' => $slider_vacancy,
									'number' => $x,
								]);
							?>
						<?php endforeach; ?>
					</div>
					<div class="take-form">
						<div class="form-for-hire">
							<div class="form-vacancy-wrap">
								<?php if ($title_form = opt('vac_form_title')) : ?>
									<h2 class="show-title-form"><?= $title_form; ?></h2>
								<?php endif; ?>
								<?php if ($subtitle_form = opt('vac_form_subtitle')) : ?>
									<h3 class="show-subtitle-form"><?= $subtitle_form; ?></h3>
								<?php endif; ?>
								<?php lang_form(['he' => '17', 'en' => '95'], 'he'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
