<?php if (isset($args['product'])) : $prod_item = $args['product'];
	$product_all = wc_get_product($prod_item);
	$link = get_permalink($product_all->get_id());
	$id = $product_all->get_id(); ?>
<div class="col-xl-4 col-sm-6 col-12 mb-3">
	<div class="card-product more-prod" data-id="<?= $id; ?>">
		<div class="prod-img-wrap">
			<a class="card-prod-img"
				<?php if (has_post_thumbnail($prod_item)) : ?>
					style="background-image: url('<?= postThumb($prod_item); ?>')"
				<?php endif; ?>
			   href="<?= $link; ?>">
			</a>
			<div class="card-buttons-wrap">
				<a href="<?= $link; ?>" class="prod-button prod-link">
					<img src="<?= ICONS  ?>view-product.png" alt="view-icon">
					<?= lang_text(['he' => 'צפייה במוצר', 'en' => 'View the product'], 'he'); ?>
				</a>
				<button role="button" class="prod-button add-prod-button add-to-cart-btn"
						data-id="<?= $id; ?>" data-quantity="1">
					<span class="not-added-yet">
						<img src="<?= ICONS ?>buy-product.png" alt="buy-icon">
						<?= lang_text(['he' => 'הוספה לסל', 'en' => 'Add to cart'], 'he'); ?>
					</span>
					<i class="fas fa-check check-icon"></i>
				</button>
			</div>
		</div>
		<div class="prod-content-wrap">
			<a class="prod-card-title" href="<?= $link; ?>">
				<?= $product_all->get_name(); ?>
			</a>
			<div class="card-prod-price">
				<?php $price = $product_all->get_regular_price();
					$price_sale = $product_all->get_sale_price(); ?>
				<span class="card-price card-sale-price">
					<?= get_woocommerce_currency_symbol().$price; ?>
				</span>
				<span class="card-base-price card-price">
					<?= get_woocommerce_currency_symbol().$price_sale; ?>
				</span>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
