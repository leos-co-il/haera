<?php
$partners = opt('partners_logo');
$title = opt('partners_title');
if ($partners) : ?>
	<div class="partners-block p-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="block-title">
						<?= $title ? $title : lang_text(['he' => 'בין הלקוחות שלנו', 'en' => 'Our clients'], 'he'); ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($partners as $n => $partner_item) : ?>
							<div class="col-xl-2 col-lg-3 col-md-4 col-6 partner-item wow fadeIn"
								 data-wow-delay="0.<?= $n + 2; ?>s">
								<img src="<?= $partner_item['url']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
