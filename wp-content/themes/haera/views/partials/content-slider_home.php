<?php if (isset($args['slider']) && $args['slider']) : ?>
	<div class="home-slider-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="row home-slider-row">
						<?php if (isset($args['slider_img']) && $args['slider_img']) : ?>
							<div class="col-lg-3">
							</div>
						<?php endif; ?>
						<div class="col-lg-9 col-12 d-flex flex-column slider-col-wrap">
							<?php if (isset($args['slider_title']) && $args['slider_title']) : ?>
								<h2 class="block-title"><?= $args['slider_title']; ?></h2>
							<?php endif; ?>
							<div class="home-slider-wrapper">
								<div class="base-slider" dir="rtl">
									<?php foreach ($args['slider'] as $slide) : ?>
										<div>
											<?php if ($slide['h_slider_title']) : ?>
											<div class="row justify-content-center">
												<div class="col">
													<h3 class="slide-title"><?= $slide['h_slider_title']; ?></h3>
												</div>
											</div>
											<?php endif; ?>
											<div class="row align-items-center slide-row-inside">
												<?php if ($slide['h_slider_item'] || $slide['h_slider_link']) : ?>
													<div class="col">
														<div class="slide-column">
															<?php if ($slide['h_slider_item']) : ?>
																<div class="slide-items-col">
																	<?php foreach ($slide['h_slider_item'] as $item) : ?>
																		<div class="slide-item">
																			<?php if ($item['sl_item_icon']) : ?>
																				<img src="<?= $item['sl_item_icon']['url']; ?>"
																					 class="slide-icon" alt="slide-icon">
																			<?php endif; ?>
																			<div class="text-slide-col">
																				<h4 class="slide-text-title"><?= $item['sl_item_title']; ?></h4>
																				<p class="base-text"><?= $item['sl_item_text']; ?></p>
																			</div>
																		</div>
																	<?php endforeach; ?>
																</div>
															<?php endif;
															if ($slide['h_slider_link']) : ?>
																<a href="<?= $slide['h_slider_link']['url']; ?>" class="base-link">
																	<?= $slide['h_slider_link']['title'] ? $slide['h_slider_link']['title'] :
																		lang_text(['he' => 'קרא עוד', 'en' => 'Read more'], 'he'); ?>
																</a>
															<?php endif; ?>
														</div>
													</div>
												<?php endif;
												if ($slide['h_slider_img']) : ?>
													<div class="col-xl-6 col-12 camp-sl-img">
														<img src="<?= $slide['h_slider_img']['url']; ?>" alt="camp-course-img"
														class="camp-course-img">
													</div>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<?php if (isset($args['slider_link']) && $args['slider_link']) : ?>
								<a href="<?= $args['slider_link']['url']; ?>" class="base-link base-link-orange mt-4 align-self-end">
									<?= $args['slider_link']['title'] ? $args['slider_link']['title'] :
										lang_text(['he' => 'קרא עוד', 'en' => 'Read more'], 'he'); ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
					<?php if (isset($args['slider_img']) && $args['slider_img']) : ?>
						<img src="<?= $args['slider_img']['url']; ?>" alt="camp-img" class="home-sl-img">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
