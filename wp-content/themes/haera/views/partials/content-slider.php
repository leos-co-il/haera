<div class="base-slider-block">
	<?php if (isset($args['img']) && $args['img']) : ?>
		<div class="slider-image-wrap wow fadeInUp"
			 style="background-image: url('<?= $args['img']['url']; ?>')"></div>
	<?php endif;
	if (isset($args['content']) && $args['content']) : ?>
		<div class="<?php echo !(isset($args['img']) && $args['img']) ? 'slider-content-max-wrap' :
			'slider-content-wrap'; ?> wow fadeInDown">
			<div class="slider-text-wrap">
				<div class="base-slider" dir="rtl">
					<?php foreach ($args['content'] as $content) : ?>
						<div>
							<div class="base-output slider-text-output">
								<?= $content['content']; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
