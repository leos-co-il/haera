<?php if ($counter = opt('counter_item')) : ?>
	<section class="counter-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 counter-block" id="start-count">
					<div class="row counter-wrap-custom justify-content-center">
						<?php foreach ($counter as $count) : ?>
							<div class="counter-item wow fadeIn">
								<h2 class="counter-number counter-num" data-from="1"
									data-to="<?= $count['number']; ?>" data-speed="1500">
									<?= $count['number']; ?>
								</h2>
								<p class="base-text text-center px-3"><?= $count['counter_desc']; ?></p>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
