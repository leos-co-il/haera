<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col">
	<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
		<a class="post-item-image" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
		</a>
		<div class="post-item-content">
			<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="base-text">
				<?= text_preview($args['post']->post_content, 10); ?>
			</p>
		</div>
		<a href="<?= $link; ?>" class="base-link post-item-link">
			<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
		</a>
	</div>
</div>
<?php endif; ?>
