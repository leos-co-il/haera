<?php if (isset($args['item']) && $args['item']) : $slider_vacancy = $args['item'];
	$x = isset($args['number']) ? $args['number'] : ''; ?>
	<div class="vac-item-col h-100">
		<div class="page-vac-item more-card" data-id="slider-vac-<?= $x; ?>">
			<div class="page-vac-title-wrap">
				<?php
				$current_id = $slider_vacancy->ID;
				?>
				<?php if ($art =  get_field('vacancy_number', $current_id)) : ?>
					<h2 class="vac-art ml-2">
						(<?= $art; ?>)
					</h2>
				<?php endif; ?>
				<h2 class="vacancy-card-title"><?= $slider_vacancy->post_title; ?></h2>
			</div>
			<div class="vac-body-wrapper">
				<div class="trigger-wrap">
					<a class="social-trigger">
						<i class="fas fa-share-alt"></i>
					</a>
					<div class="all-socials item-socials" id="show-socials">
						<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<i class="fas fa-envelope"></i>
						</a>
						<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap social-item">
							<i class="fab fa-whatsapp"></i>
						</a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $current_id; ?>&title=&summary=&source="
						   target="_blank"
						   class="socials-wrap social-item">
							<i class="fab fa-linkedin-in"></i>
						</a>
					</div>
				</div>
				<div class="page-vac-item-body">
					<div class="info-vac-wrap">
						<?php
						$positions_slider = get_the_terms($current_id, 'position');
						$vacancy_format_slider = get_the_terms($current_id, 'vacancy_format');
						$regions_slider = get_the_terms($current_id, 'region');
						?>
						<?php if ($positions_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title static">
									<?= lang_text(['he' => 'תפקיד:', 'en' => 'Position: '], 'he'); ?>
								</h2>
								<?php foreach ($positions_slider as $key => $position) : ?>
									<h2 class="page-vac-title"><?= $position->name; ?></h2>
									<?php if ($key !== (count($positions_slider) - 1)) : ?>
										<h2 class="page-vac-title"> ,</h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php if ($vacancy_format_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title static">
									<?= lang_text(['he' => 'היקף משרה:', 'en' => 'Form of employment'], 'he'); ?>
								</h2>
								<?php foreach ($vacancy_format_slider as $i => $format) : ?>
									<h2 class="page-vac-title"><?= $format->name; ?></h2>
									<?php if ($i !== (count($vacancy_format_slider) - 1)) : ?>
										<h2 class="page-vac-title"> ,</h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php if ($regions_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title static">
									<?= lang_text(['he' => 'מיקום:', 'en' => 'Location: '], 'he'); ?>
								</h2>
								<?php foreach ($regions_slider as $k => $region) : ?>
									<?php if ($k !== (count($regions_slider) - 1)) : ?>
										<h2 class="page-vac-title"> ,</h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="vacancy-body-output">
						<?= text_preview($slider_vacancy->post_content, 30); ?>
					</div>
					<a class="read-more-link" href="<?php the_permalink($slider_vacancy); ?>">
						<?= lang_text(['he' => 'קרא עוד...', 'en' => 'Read more...'], 'he'); ?>
					</a>
				</div>
				<a class="get-vacancy" data-vacancy="<?= $slider_vacancy->post_title; ?>"
				   data-target=".form-wrap-<?= $x; ?>">
					<span>
						<?= lang_text(['he' => 'להגשת מועמדות', 'en' => 'To apply'], 'he'); ?>
					</span>
				</a>
			</div>
			<div class="price-form-wrapper form-wrap-<?= $x; ?>">
				<span class="close-form" data-target=".form-wrap-<?= $x; ?>">
					<?= svg_simple(ICONS.'vacancy-form-close.svg'); ?>
				</span>
				<div class="put-form-here">
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
