<section class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-center">
					<?php if ($logo_form = opt('logo')) : ?>
						<div class="col-lg-5 col-sm-7 col-9 mb-3">
							<img src="<?= $logo_form['url']; ?>" alt="logo">
						</div>
					<?php endif; ?>
					<div class="col-12">
						<?php if ($foo_f_title = opt('rep_form_title')) : ?>
							<div class="foo-form-text"><?= $foo_f_title; ?></div>
						<?php endif;
						lang_form(['he' => '16', 'en' => '97'], '16') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
