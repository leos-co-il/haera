<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$tel_2 = opt('tel_2');
$mail = opt('mail');
$mail_sales = opt('mail_sales');
$mail_job = opt('mail_job');
$address = opt('address');
$map = opt('map_image');
?>

<article class="page-body mt-5 mb-5">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<div class="col-12 d-flex flex-column justify-content-center align-items-center">
					<h1 class="block-title mb-4"><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="row justify-content-center">
						<?php if ($tel || $tel_2) : ?>
							<div class="col-lg-4 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
								 data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<h3 class="contact-info-title">
									<?= lang_text(['he' => 'טלפונים:', 'en' => 'Phone numbers:'], 'he'); ?>
								</h3>
								<?php if ($tel) : ?>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										 <?= lang_text(['he' => 'ראשי:', 'en' => 'Main:'], 'he').$tel; ?>
									</a>
								<?php endif;
								if ($tel_2) : ?>
									<a href="tel:<?= $tel_2; ?>" class="contact-info">
										<?= $tel_2; ?>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ($mail || $mail_sales || $mail_job) : ?>
							<div class="contact-item col-lg-4 col-sm-6 col-11 contact-item-link wow zoomIn"
								 data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<h3 class="contact-info-title">
									<?= lang_text(['he' => 'מייל:', 'en' => 'Emails:'], 'he');  ?>
								</h3>
								<?php if ($mail) : ?>
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= $mail; ?>
									</a>
								<?php endif;
								if ($mail_sales) : ?>
									<a href="mailto:<?= $mail_sales; ?>" class="contact-info">
										<?= lang_text(['he' => 'מכירות:', 'en' => 'Sales:'], 'he').$mail_sales; ?>
									</a>
								<?php endif;
								if ($mail_job) : ?>
									<a href="mailto:<?= $mail_job; ?>" class="contact-info">
										<?= lang_text(['he' => 'דרושים:', 'en' => 'Jobs:'], 'he').$mail_job; ?>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ($address) : ?>
							<a href="https://www.waze.com/ul?q=<?= $address; ?>"
							   class="contact-item-link contact-item col-lg-4 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</div>
								<h3 class="contact-info-title">
									<?= lang_text(['he' => 'כתובת:', 'en' => 'Address:'], 'he');  ?>
								</h3>
								<p class="contact-info">
									<?= $address; ?>
								</p>
							</a>
						<?php endif; ?>
					</div>
					<div class="contact-form-wrap wow zoomIn" data-wow-delay="0.4s">
						<?php if ($logo_pop = opt('logo')) : ?>
							<div class="row justify-content-center mb-4">
								<div class="col-xl-4 col-lg-5 col-8">
									<a href="/" class="logo">
										<img src="<?= $logo_pop['url'] ?>" alt="logo">
									</a>
								</div>
							</div>
						<?php endif;?>
						<div class="row justify-content-center align-items-center mb-4">
							<?php if ($f_title = $fields['contact_form_title']) : ?>
								<div class="col-auto">
									<h2 class="pop-form-title"><?= $f_title; ?></h2>
								</div>
							<?php endif; ?>
							<?php if ($f_text = $fields['contact_form_text']) : ?>
								<div class="col-auto">
									<h2 class="pop-form-title font-weight-normal">
										<?= $f_text; ?>
									</h2>
								</div>
							<?php endif; ?>
						</div>
						<div class="contact-page-form">
							<?php lang_form(['he' => '15', 'en' => '94'], '15'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="base-output-contact">
						<?php the_content(); ?>
					</div>
				</div>
				<?php if ($map) : ?>
					<div class="col-12">
						<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
							<img src="<?= $map['url']; ?>" alt="map">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
