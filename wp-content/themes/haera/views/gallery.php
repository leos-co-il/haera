<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();
?>

<article class="page-body gallery-body">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="row justify-content-start breadcrumbs-custom mb-3">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center">
			<div class="col-12 mb-3">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output text-center"><?php the_content(); ?></div>
			</div>
		</div>
		<?php if ($fields['galleries']) : ?>
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs row" id="type-gallery-tab" role="tablist">
					<?php foreach ($fields['galleries'] as $x => $gallery) : ?>
					<li class="nav-item tab-style col mb-3">
						<a class="nav-link main-nav-link <?= ($x === 0) ? 'active' : ''; ?>" id="<?= $x; ?>-images-tab" data-toggle="tab" href="#images-tab-<?= $x; ?>"
						   role="tab" aria-controls="type" aria-selected="true">
							<?= $gallery['gallery_name']; ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="row tab-content mb-5">
			<?php foreach ($fields['galleries'] as $x => $gallery) : ?>
				<div class="col-12 tab-pane fade <?= ($x === 0) ? 'show active' : ''; ?>" id="images-tab-<?= $x; ?>" role="tabpanel" aria-labelledby="<?= $x; ?>-images-tab">
					<?php if ($gallery['sub_galleries']) : ?>
						<div class="row">
							<div class="col-12">
								<ul class="nav nav-tabs tabs-main-gallery row" id="home-gallery-tab" role="tablist">
									<?php foreach ($gallery['sub_galleries'] as $title_num => $title) : ?>
										<li class="nav-item tab-style-col col-auto">
											<a class="nav-link sub-tab <?= ($title_num === 0) ? 'active' : ''; ?>"
											   id="gallery-<?= $x.$title_num; ?>-tab" data-toggle="tab" href="#gallery-<?= $x.$title_num; ?>"
											   role="tab" aria-controls="home" aria-selected="true">
												<?= $title['sub_gallery_name']; ?>
											</a>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<div class="col-12">
								<div class="tab-content">
									<?php foreach ($gallery['sub_galleries'] as $gal_num => $gal_item) : ?>
										<div class="tab-pane fade <?= ($gal_num === 0) ? 'show active' : ''; ?>" id="gallery-<?= $x.$gal_num; ?>" role="tabpanel" aria-labelledby="gallery-<?= $x.$gal_num; ?>-tab">
											<div class="row gallery-row justify-content-center" id="gallery-id-<?= $x.$gal_num; ?>">
												<?php if ($gal_item['sub_gallery_items']) : foreach ($gal_item['sub_gallery_items'] as $chunk_num => $chunk) : ?>
													<div class="col-gal col-xl-3 col-lg-4 col-sm-6 col-12 <?= $chunk_num <= 11 ? 'show' : ''; ?>">
														<div class="gallery-item" style="background-image: url('<?= $chunk['url']; ?>')">
															<a class="gallery-overlay d-flex justify-content-center align-items-center" data-lightbox="img"
															   href="<?= $chunk['url']; ?>">
																<span class="overlay-plus">+</span>
															</a>
														</div>
													</div>
												<?php endforeach; ?>
												<?php if (count($gal_item['sub_gallery_items']) > 12) : ?>
													<div class="col-auto more-link load-more-img mt-4"
														 data-id="gallery-id-<?= $x.$gal_num; ?>">
														<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>
													</div>
												<?php endif; endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/video', 'modal'); ?>
<div class="form-without-margins">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($fields['single_slider_seo']) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
if ($all_faq = $fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_desc' => $fields['faq_text'],
			'faq' => $all_faq,
		]);
}
get_footer(); ?>

