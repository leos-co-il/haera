<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();

?>

<article class="page-body about-page">
	<div class="container-fluid">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="row justify-content-start">
				<div class="col-auto">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center row-about">
			<div class="col-xl-10 col-11 col-about-content">
				<div class="row align-items-start justify-content-center reverse-row-about">
					<div class="<?php echo !(has_post_thumbnail()) ? 'col-12' :
							'col-lg-6 col-12'; ?>">
						<div class="text-wrap">
							<div class="base-output base-single-output">
								<h2 class="font-weight-bold mb-3"><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
					<?php if (has_post_thumbnail()) : ?>
						<div class="col-lg-6 col-12">
							<img src="<?= postThumb(); ?>" alt="about-img" class="about-page-image">
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if ($fields['about_content_repeat']) : ?>
				<div class="col-xl-10 col-11 col-about-content">
					<?php foreach ($fields['about_content_repeat'] as $item) : ?>
						<div class="row align-items-start justify-content-center reverse-row-about">
							<?php if ($item['about_text']) : ?>
								<div class="<?php echo !($item['about_img']) ? 'col-12' :
										'col-lg-6 col-12'; ?>">
									<div class="text-wrap">
										<div class="base-output base-single-output"><?= $item['about_text']; ?></div>
									</div>
								</div>
							<?php endif;
							if ($item['about_img']) : ?>
								<div class="col-lg-6 col-12">
									<img src="<?= $item['about_img']['url']; ?>" alt="about-img" class="about-page-image">
								</div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'counter');
if ($fields['about_team_item']) :
	$pos = lang_text(['he' => 'תפקיד: ', 'en' => 'Position: '], 'he'); ?>
	<section class="about-team-block mt-5">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<?php if ($fields['about_team_title']) : ?>
					<div class="col-auto mb-3">
						<div class="block-title">
							<?= $fields['about_team_title']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['about_team_item'] as $team_item) : ?>
					<div class="col-lg-3 col-sm-6 col-12 worker-col">
						<?php if ($team_item['worker_img']) : ?>
						<div class="about-team-img" style="background-image: url('<?= $team_item['worker_img']['url'];?>')">
						</div>
						<?php endif; ?>
						<h3 class="mid-text"><?= $team_item['worker_name']; ?></h3>
						<h3 class="mid-text"><?= $pos.$team_item['worker_position']; ?></h3>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['why_us_item']) : ?>
	<section class="why-us-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-5 d-flex align-items-center why-content-col">
					<div class="row row-why-us">
						<div class="col-12 step-item-main">
							<div class="row align-items-start">
								<div class="col-auto number-circle-col reason-col">
									<h3 class="reason-number"><?= count($fields['why_us_item']); ?></h3>
								</div>
								<div class="col process-content-col">
									<div class="process-wrap">
										<h3 class="reason-small-title">
											<?= lang_text(['he' => 'סיבות', 'en' => 'Reasons'], 'he'); ?>
										</h3>
										<h3 class="reason-title">
											<?= lang_text(['he' => 'למה לבחור בנו', 'en' => 'Why choose us'], 'he'); ?>
										</h3>
									</div>
								</div>
							</div>
						</div>
						<?php foreach ($fields['why_us_item'] as $num => $why_item) : ?>
							<div class="col-12 step-item wow fadeInUp"
								 data-wow-delay="0.<?= $num; ?>s">
								<div class="row align-items-start">
									<div class="col-sm-auto col-12 number-circle-col">
										<div class="number-circle">
											<?php if ($why_item['why_icon']) : ?>
												<img src="<?= $why_item['why_icon']['url']; ?>">
											<?php endif; ?>
										</div>
									</div>
									<div class="col process-content-col">
										<div class="process-wrap">
											<h3 class="why-title">
												<?= $why_item['why_title']; ?>
											</h3>
											<div class="why-text">
												<?= $why_item['why_text']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php if ($fields['why_us_img']) : ?>
					<div class="col-xl-5 why-img-col">
						<img src="<?= $fields['why_us_img']['url']; ?>" alt="why-us">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'partners');
if ($slider_seo = $fields['single_slider_seo']) :
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $slider_seo,
					'img' => $fields['slider_img'],
			]);
endif;
if ($all_faq = $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $all_faq,
			]);
endif;
get_footer(); ?>
