<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$post_type = $fields['type'] ? $fields['type'] : 'post';
switch ($post_type) {
	case 'post':
		$type_cat = 'category';
		break;
	case 'service':
		$type_cat = 'service_cat';
		break;
	case 'course':
		$type_cat = 'course_cat';
		break;
	case 'class':
		$type_cat = 'class_cat';
		break;
	case 'camp':
		$type_cat = 'camp_cat';
		break;
	default:
		$type_cat = 'post';
		$taxname = 'category';
}
$posts = get_posts([
	'numberposts' => 8,
	'post_type' => $post_type,
	'suppress_filters' => false
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => $post_type,
	'suppress_filters' => false
]);
$cats = get_terms([
	'taxonomy' => $type_cat,
	'hide_empty' => false,
]);
?>
<article class="page-body">
	<div class="body-output">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-11 col-12">
					<div class="row">
						<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
					<div class="row justify-content-center mt-3">
						<div class="col">
							<h1 class="block-title"><?php the_title(); ?></h1>
						</div>
					</div>
					<?php if ($posts) : ?>
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($posts as $post) {
								get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]);
							} ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (count($posts_all) > 8) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="<?= $post_type; ?>"
						 data-tax-type="<?= ($post_type === 'class') ? 'service_cat': 'category; '?>">
						<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<div class="form-without-margins">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($fields['single_slider_seo']) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
if ($all_faq = $fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $all_faq,
			]);
}
get_footer(); ?>

