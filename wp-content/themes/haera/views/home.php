<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$sliders = ['h_slider_1', 'h_slider_2', 'h_slider_3', 'h_slider_4', 'h_slider_5'];
?>

<?php if ($mainLinkVideo = $fields['home_main_video']) : ?>
	<section class="main-video-wrapper">
		<video poster="<?= $fields['poster_main'] ? $fields['poster_main']['url'] : ''; ?>" onclick="this.paused ? this.play() : this.pause();">
			<source src="<?= $mainLinkVideo['url']; ?>" type="video/mp4">
		</video>
		<?= svg_simple(ICONS.'play-button.svg'); ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'counter'); ?>
<section class="sliders-block">
	<?php foreach ($sliders as $x => $slide) {
	if ($fields[$slide]) {
		get_template_part('views/partials/content', 'slider_home', [
				'slider' => $fields[$slide],
				'slider_link' => $fields['h_slider_link_'.($x + 1)],
				'slider_img' => $fields['h_slider_img_'.($x + 1)],
				'slider_title' => $fields['h_slider_title_'.($x + 1)],
		]);
	}
}?>
</section>
<?php if ($fields['home_offer_img']) : ?>
	<section class="home-offer-block">
		<img src="<?= $fields['home_offer_img']['url']; ?>" alt="offer-img" class="offer-img-home">
		<div class="home-offer-block-inside">
			<?php if ($logo = opt('logo')) : ?>
				<a href="/" class="logo logo-offer">
					<img src="<?= $logo['url'] ?>" alt="logo">
				</a>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_prod_slider']) : ?>
	<section class="products-slider-block">
		<div class="container-fluid">
			<?php if ($fields['h_prod_slider_title']) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="block-title mb-5"><?= $fields['h_prod_slider_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-sm-11 col-10">
					<div class="prod-slider" dir="rtl">
						<?php foreach ($fields['home_prod_slider'] as $product) : ?>
							<div class="slide-product">
								<?php get_template_part('views/partials/card', 'product_item',
								[
										'product' => $product,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['h_prod_slider_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_prod_slider_link']['url']; ?>" class="base-link">
							<?= $fields['h_prod_slider_link']['title'] ? $fields['h_prod_slider_link']['title'] :
							lang_text(['he' => 'לכל הערכות', 'en' => 'View more'], 'he'); ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_about_img'] || $fields['h_about_text']) : ?>
	<div class="base-slider-block">
		<?php if ($fields['h_about_img']) : ?>
			<div class="slider-image-wrap wow fadeInUp"
				 style="background-image: url('<?= $fields['h_about_img']['url']; ?>')"></div>
		<?php endif;
		if ($fields['h_about_text']) : ?>
			<div class="<?php echo !($fields['h_about_img']) ? 'slider-content-max-wrap' :
					'slider-content-wrap'; ?> wow fadeInDown">
				<div class="slider-text-wrap">
					<div class="base-output slider-text-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<div class="row justify-content-end">
							<div class="col-auto mt-4">
								<a href="<?= $fields['h_about_link']['url']; ?>" class="base-link">
									<?= $fields['h_about_link']['title'] ? $fields['h_about_link']['title'] :
											lang_text(['he' => 'קרא עוד עלינו', 'en' => 'Read more about us'], 'he'); ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif;
get_template_part('views/partials/repeat', 'partners');
if ($fields['timetable_slider'] || $fields['news_slider']) : ?>
	<section class="news-block-main">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-11 col-12 news-block-wrap">
					<div class="row justify-content-center">
						<?php if ($fields['timetable_slider']) :
							$chunks = array_chunk($fields['timetable_slider'], 4); ?>
							<div class="col-xl-7 col-12">
								<?php if ($fields['timetable_slider_title']) : ?>
									<h2 class="news-base-title"><?= $fields['timetable_slider_title']; ?></h2>
								<?php endif; ?>
								<div class="events-slider class-slider">
									<?php foreach ($chunks as $chunk) : ?>
										<div class="slider-chunk">
											<div class="chunks-wrapper">
												<?php foreach ($chunk as $item) : ?>
													<div class="event-item-wrap">
														<div class="event-item">
															<p class="news-item-text"><?= $item['event_text']; ?></p>
														</div>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif;
						if ($fields['news_slider']) : ?>
							<div class="col-xl-5 col-12 news-col-home">
								<?php if ($fields['news_slider_title']) : ?>
									<h2 class="news-base-title"><?= $fields['news_slider_title']; ?></h2>
								<?php endif; ?>
								<div class="news-slider class-slider">
									<?php foreach ($fields['news_slider'] as $news) : ?>
										<div >
											<div class="slide-news-item">
												<p class="news-item-text"><?= $news['new_slide_text']; ?></p>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-11 col-12">
					<?php if ($fields['timetable_news_slider_link']) : ?>
						<div class="row justify-content-end">
							<div class="col-auto">
								<a href="<?= $fields['timetable_news_slider_link']['url']; ?>" class="base-link">
									<?= $fields['timetable_news_slider_link']['title'] ? $fields['timetable_news_slider_link']['title'] :
											lang_text(['he' => 'לכל העדכונים', 'en' => 'For all updates'], 'he'); ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['review_item']) : ?>
	<section class="reviews-block">
		<div class="container-fluid">
			<?php if ($fields['reviews_block_title']) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="block-title mb-5"><?= $fields['reviews_block_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-md-10 col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['review_item'] as $review) : ?>
							<div class="slide-rev">
								<div class="review-item">
									<div class="rev-content">
										<h3 class="review-name"><?= $review['rev_name']; ?></h3>
										<p class="review-prev">
											<?= text_preview($review['rev_text'], '12'); ?>
										</p>
										<div class="rev-pop-trigger">
											+
											<div class="hidden-review">
												<div class="base-output">
													<?= $review['rev_text']; ?>
												</div>
											</div>
										</div>
									</div>
									<?php if ($review['rev_img']) : ?>
										<div class="review-image-wrap">
											<div class="review-img">
												<img src="<?= $review['rev_img']['url']; ?>">
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Reviews pop-up-->
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['home_content_block_text']) : ?>
	<section class="content-part-block">
		<div class="content-part">
			<?php if ($fields['home_content_block_img_1']) : ?>
				<img src="<?= $fields['home_content_block_img_1']['url']; ?>" alt="img-worker" class="img-worker-home wow fadeInUp">
			<?php endif; ?>
		</div>
		<div class="content-part-big">
			<div class="base-output slider-text-output text-center">
				<?= $fields['home_content_block_text']; ?>
			</div>
			<?php if ($fields['home_content_block_link']) : ?>
				<a href="<?= $fields['home_content_block_link']['url']; ?>" class="base-link mt-4">
					<?= $fields['home_content_block_link']['title'] ? $fields['home_content_block_link']['title'] :
							lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="content-part">
			<?php if ($fields['home_content_block_img_2']) : ?>
				<img src="<?= $fields['home_content_block_img_2']['url']; ?>" alt="img-product" class="wow fadeIn">
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_images_slider']) : ?>
	<section class="reviews-block home-gallery-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-md-10 col-12">
					<div class="prod-slider" dir="rtl">
						<?php foreach ($fields['home_images_slider'] as $img) : ?>
							<div class="p-2">
								<a href="<?= $img['url']; ?>" style="background-image: url('<?= $img['url']; ?>')"
								data-lightbox="slider-gallery-img" class="slide-gallery-item"></a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<div class="base-slider-block flex-row-reverse m-0 home-form-block">
	<img src="<?= IMG ?>top-line.png" class="top-line">
	<?php if ($fields['home_form_img']) : ?>
		<div class="slider-image-wrap wow fadeInUp"
			 style="background-image: url('<?= $fields['home_form_img']['url']; ?>')"></div>
	<?php endif; ?>
	<div class="<?php echo !($fields['home_form_img']) ? 'slider-content-max-wrap' :
			'slider-content-wrap'; ?> wow fadeInDown">
		<div class="slider-text-wrap form-home-wrap">
			<?php if ($logo_pop = opt('logo_dark')) : ?>
				<a href="/" class="logo mb-4 logo-home-form">
					<img src="<?= $logo_pop['url'] ?>" alt="logo">
				</a>
			<?php endif;
			if ($fields['home_form_title']) : ?>
				<h2 class="home-form-title"><?= $fields['home_form_title']; ?></h2>
			<?php endif;
			if ($fields['home_form_text']) : ?>
				<h3 class="home-form-subtitle"><?= $fields['home_form_text']; ?></h3>
			<?php endif;
			lang_form(['he' => '14', 'en' => '96'], '14') ; ?>
		</div>
	</div>
</div>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
if ($fields['home_reg_text']) : ?>
	<section class="home-registration-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-md-10 col-sm-11 col-12">
					<div class="row justify-content-center">
						<div class="col-lg-6 reg-col-content">
							<?php if ($fields['home_reg_text']) : ?>
								<div class="base-output reg-text-output">
									<?= $fields['home_reg_text']; ?>
								</div>
							<?php endif;
							if ($fields['home_reg_link']) : ?>
								<a href="<?= $fields['home_reg_link']['url']; ?>" class="base-link mt-4">
									<?= $fields['home_reg_link']['title'] ? $fields['home_reg_link']['title'] :
											lang_text(['he' => 'להרשמה', 'en' => 'To register'], 'he'); ?>
								</a>
							<?php endif; ?>
						</div>
						<?php if ($fields['home_reg_img']) : ?>
							<div class="col-lg-6 reg-img-col">
								<img src="<?= $fields['home_reg_img']['url']; ?>" alt="registration-img">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_vac_link'] || $fields['home_vac_title'] || $fields['home_vac_subtitle']) : ?>
	<section class="vacancy-form-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg col-sm-12 vac-img-col-r">
					<?php if ($fields['home_vac_img_1']) : ?>
						<img src="<?= $fields['home_vac_img_1']['url']; ?>" alt="human">
					<?php endif; ?>
				</div>
				<div class="col-xl-5 col-lg-7 col-12 home-form-vac">
					<?php if ($logo_pop = opt('logo_dark')) : ?>
					<a href="/" class="logo mb-4 logo-vac-form">
						<img src="<?= $logo_pop['url'] ?>" alt="logo">
					</a>
					<?php endif;
					if ($fields['home_vac_title']) : ?>
						<h2 class="home-vac-form-title">
							<?= $fields['home_vac_title']; ?>
						</h2>
					<?php endif;
					if ($fields['home_vac_subtitle']) : ?>
						<h3 class="home-vac-form-subtitle">
							<?= $fields['home_vac_subtitle']; ?>
						</h3>
					<?php endif;
					if ($fields['home_vac_link']) : ?>
						<a href="<?= $fields['home_vac_link']['url']; ?>" class="base-link">
							<?= $fields['home_vac_link']['title'] ? $fields['home_vac_link']['title'] :
									lang_text(['he' => 'למשרות שלנו', 'en' => 'To our jobs'], 'he'); ?>
						</a>
					<?php endif; ?>
				</div>
				<div class="col-lg vac-img-col-l">
					<?php if ($fields['home_vac_img_2']) : ?>
						<img src="<?= $fields['home_vac_img_2']['url']; ?>" alt="human">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php if ($all_faq = $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $all_faq,
			]);
endif;
get_footer(); ?>

