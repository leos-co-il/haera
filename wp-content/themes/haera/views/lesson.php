<?php
/*
Template Name: מערך שיעור
*/

get_header();
$fields = get_fields();

?>

<article class="page-body about-page">
	<div class="container-fluid">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="row justify-content-start">
				<div class="col-auto">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center row-about">
			<div class="col-xl-10 col-11 col-about-content">
				<div class="row align-items-start justify-content-center reverse-row-about">
					<div class="<?php echo !(has_post_thumbnail()) ? 'col-12' :
						'col-lg-6 col-12'; ?>">
						<div class="text-wrap">
							<div class="base-output base-single-output">
								<h2 class="font-weight-bold mb-3"><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
					<?php if (has_post_thumbnail()) : ?>
						<div class="col-lg-6 col-12">
							<img src="<?= postThumb(); ?>" alt="about-img" class="about-page-image">
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if ($fields['lesson_content_repeat']) : ?>
				<div class="col-xl-10 col-11 col-about-content">
					<?php foreach ($fields['lesson_content_repeat'] as $item) : ?>
						<div class="row align-items-start justify-content-center reverse-row-about">
							<?php if ($item['lesson_text']) : ?>
								<div class="<?php echo !($item['lesson_img']) ? 'col-12' :
									'col-lg-6 col-12'; ?>">
									<div class="text-wrap">
										<div class="base-output base-single-output"><?= $item['lesson_text']; ?></div>
									</div>
								</div>
							<?php endif;
							if ($item['lesson_img']) : ?>
								<div class="col-lg-6 col-12">
									<img src="<?= $item['lesson_img']['url']; ?>" alt="about-img" class="about-page-image">
								</div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($fields['lesson_videos']) : ?>
		<div class="container-fluid my-5">
			<?php if ($fields['lesson_videos_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title mb-4"><?= $fields['lesson_videos_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['lesson_videos'] as $item) : ?>
							<div class="col-gal col-lg-3 col-sm-6 col-12 d-block">
								<div class="gallery-item" style="background-image: url('<?= getYoutubeThumb($item['lesson_video_link']); ?>')">
									<span class="overlay-video play-button" data-video="<?= getYoutubeId($item['lesson_video_link']); ?>">
										<?= svg_simple(ICONS.'play-button.svg'); ?>
									</span>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	get_template_part('views/partials/video', 'modal');
	if ($fields['lesson_gallery']) : ?>
		<div class="container-fluid my-5">
			<?php if ($fields['lesson_gallery_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title mb-4"><?= $fields['lesson_gallery_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['lesson_gallery'] as $u => $item) : ?>
							<div class="col-gal col-xl-3 col-lg-4 col-sm-6 col-12 d-block wow fadeIn"
							data-wow-delay="0.<?= $u + 1; ?>s">
								<div class="gallery-item" style="background-image: url('<?= $item['url']; ?>')">
									<a class="gallery-overlay d-flex justify-content-center align-items-center" data-lightbox="img"
									   href="<?= $item['url']; ?>">
										<span class="overlay-plus">+</span>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	if ($fields['lesson_file_item']) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11 files-block">
					<?php foreach ($fields['lesson_file_item'] as $file_item) : ?>
						<div class="row pdf-item-wrap align-items-center">
							<div class="col-sm-auto col-6 pdf-col">
								<div class="pdf-wrap">
									<?= svg_simple(ICONS.'pdf.svg'); ?>
								</div>
							</div>
							<div class="col-sm col-12 file-col">
								<h3 class="pdf-title"><?= $file_item['lesson_file_title']; ?></h3>
								<p class="pdf-text"><?= $file_item['lesson_file_text']; ?></p>
							</div>
							<div class="col-sm-auto col-6 down-col">
								<?php if ($file_item['lesson_file']) : ?>
								<a class="down-wrap" href="<?= $file_item['lesson_file']['url']; ?>">
									<img src="<?= ICONS ?>download.png" alt="download">
								</a>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
if ($all_faq = $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_desc' => $fields['faq_text'],
			'faq' => $all_faq,
		]);
endif;
get_footer(); ?>
