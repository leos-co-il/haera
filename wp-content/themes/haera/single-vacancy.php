<?php
the_post();
get_header();
$fields = get_fields();
$title = get_the_title();
$current_id = get_the_ID();
$positions_slider = get_the_terms($current_id, 'position');
$vacancy_format_slider = get_the_terms($current_id, 'vacancy_format');
$regions_slider = get_the_terms($current_id, 'region');
?>

<article class="page-body vacancy-page">
	<div class="container-fluid">
		<div class="row justify-content-center mt-3">
			<div class="col-12 breadcrumbs-custom mb-3">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
			<div class="col-xl-7 col-lg-8 col-md-9 col-sm-11 col-12 d-flex flex-column align-items-center">
				<div class="base-vac-body">
					<h1 class="base-vacancy-title mb-3"><?= $title; ?></h1>
					<div class="info-vac-wrap">
						<?php if ($positions_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title">
									<?= lang_text(['he' => 'תפקיד:', 'en' => 'Position: '], 'he'); ?>
								</h2>
								<?php foreach ($positions_slider as $key => $position) : ?>
									<h2 class="page-vac-title"><?= $position->name; ?></h2>
									<?php if ($key !== (count($positions_slider) - 1)) : ?>
										<h2 class="page-vac-title"> ,</h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php if ($vacancy_format_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title">
									<?= lang_text(['he' => 'היקף משרה:', 'en' => 'Form of employment: '], 'he'); ?>
								</h2>
								<?php foreach ($vacancy_format_slider as $i => $format) : ?>
									<h2 class="page-vac-title"><?= $format->name; ?></h2>
									<?php if ($i !== (count($vacancy_format_slider) - 1)) : ?>
										<h2 class="page-vac-title"></h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php if ($regions_slider) : ?>
							<div class="misra-wrap">
								<h2 class="page-vac-title">
									<?= lang_text(['he' => 'מיקום:', 'en' => 'Location: '], 'he'); ?>
								</h2>
								<?php foreach ($regions_slider as $k => $region) : ?>
									<h2 class="page-vac-title"><?= $region->name; ?></h2>
									<?php if ($k !== (count($regions_slider) - 1)) : ?>
										<h2 class="page-vac-title"> ,</h2>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<h3 class="page-vac-title">
						<?= lang_text(['he' => 'תיאור המשרה:', 'en' => 'Job description:'], 'he'); ?>
					</h3>
					<div class="base-output mb-4">
						<?php the_content(); ?>
					</div>
					<?php if ($to_do = $fields['requirements']) : ?>
						<h3 class="page-vac-title">
							<?= lang_text(['he' => 'דרישות התפקיד:', 'en' => 'Requirements:'], 'he'); ?>
							</h3>
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-center w-100">
					<div class="col-auto va-col-page">
						<div class="trigger-wrap">
							<a class="social-trigger">
								<i class="fas fa-share-alt"></i>
							</a>
							<div class="all-socials item-socials" id="show-socials">
								<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
									<i class="fas fa-envelope"></i>
								</a>
								<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
								   target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
								   class="socials-wrap social-item">
									<i class="fab fa-whatsapp"></i>
								</a>
								<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $current_id; ?>&title=&summary=&source="
								   target="_blank"
								   class="socials-wrap social-item">
									<i class="fab fa-linkedin-in"></i>
								</a>
							</div>
						</div>
						<a class="get-vacancy" data-vacancy="<?= $title; ?>"
						   data-target=".form-wrap-vac">
							<span>
								<?= lang_text(['he' => 'להגשת מועמדות', 'en' => 'To apply'], 'he'); ?>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="take-form">
	<div class="form-for-hire">
		<div class="form-vacancy-wrap">
			<?php if ($title_form = opt('vac_form_title')) : ?>
				<h2 class="show-title-form"><?= $title_form; ?></h2>
			<?php endif; ?>
			<?php if ($subtitle_form = opt('vac_form_subtitle')) : ?>
				<h3 class="show-subtitle-form"><?= $subtitle_form; ?></h3>
			<?php endif; ?>
			<?php lang_form(['he' => '17', 'en' => '95'], 'he'); ?>
		</div>
	</div>
</div>
<div class="vac-page-pop">
	<div class="price-form-wrapper form-wrap-vac">
		<span class="close-form" data-target=".form-wrap-vac">
			<?= svg_simple(ICONS.'vacancy-form-close.svg'); ?>
		</span>
		<div class="put-form-here">
		</div>
	</div>
</div>
<?php get_template_part('views/partials/repeat', 'form');
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'vacancy_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'vacancy',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'vacancy_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_vacs']) {
	$samePosts = $fields['same_vacs'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'vacancy',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) :
	get_template_part('views/partials/content', 'slider_vacancy',
		[
			'content' => $samePosts,
			'title' => $fields['same_title'] ? $fields['same_title'] : 'משרות נוספות באותו תחום',
		]);
endif;
if ($slider_seo = $fields['single_slider_seo']) :
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $slider_seo,
			'img' => $fields['slider_img'],
		]);
endif;
if ($all_faq = $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_desc' => $fields['faq_text'],
			'faq' => $all_faq,
		]);
endif;
get_footer(); ?>
