<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
?>
<div class="container-fluid shop-container page-body mt-5">
	<div class="row justify-content-center">
		<div class="col-sm-11 col-12">
			<div class="home-products" id="home-shop">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="row justify-content-center">
							<div class="col-xl-3 col-lg-4 col-12 justify-content-start breads-woo">
								<?php do_action('woo_custom_breadcrumb'); ?>
							</div>
							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
								<div class="col-xl-9 col-lg-8 col-12">
									<h1 class="block-title text-center mb-4">
										<?php woocommerce_page_title(); ?>
									</h1>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4  col-12">
						<div class="accordion" id="accordion-cats">
							<div class="form-check-wrap">
								<?php foreach($cats as $number => $parent_term) : ?>
									<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
									<div class="card">
										<div class="card-header" >
											<a class="prod-cat-title" href="<?= get_category_link($parent_term); ?>">
												<?= $parent_term->name; ?>
											</a>
											<?php if (!empty($term_children)) : ?>
												<button class="btn btn-link accordion-trigger" type="button">
													<span class="plus-icon-acc">+</span>
													<span class="minus-icon-acc">-</span>
												</button>
											<?php endif; ?>
										</div>
										<div id="collapse-<?= $number; ?>" class="collapse body-acc" >
											<div class="card-body">
												<?php if ($term_children) :
													foreach ($term_children as $x => $child) : $term = get_term_by( 'id', $child, 'product_cat' ); ?>
														<div class="subcat-item">
															<a href="<?= get_term_link($term); ?>">
																<?= $term->name; ?>
															</a>
														</div>
													<?php endforeach;
												endif; ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
					<div class="col-xl-9 col-lg-8 col-12 col-parent">
						<?php if (woocommerce_product_loop()): ?>
							<div class="row justify-content-center put-here-prods">
								<?php if ( wc_get_loop_prop( 'total' ) ) {
									while ( have_posts() ) {
										the_post();
										/**
										 * Hook: woocommerce_shop_loop.
										 */
										do_action( 'woocommerce_shop_loop' );
										get_template_part('views/partials/card', 'product',
												[
														'product' => get_the_ID(),
												]);
									}
								}
								wp_reset_postdata(); ?>
							</div>
							<?php if( $paged < $max_pages ) : ?>
								<div class="row justify-content-center mt-4 mb-5 ">
									<div class="col-auto">
										<div id="loadmore" data-load="<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>"
										data-loading="<?= lang_text(['he' => 'טוען', 'en' => 'Loading'], 'he'); ?>">
											<a href="#" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '" class="more-link">
												<?= lang_text(['he' => 'טען עוד...', 'en' => 'Load more'], 'he'); ?>
											</a>
										</div>
									</div>
								</div>
							<?php endif; ?>
						<?php else: ?>
							<?php

							/**
							 * Hook: woocommerce_no_products_found.
							 *
							 * @hooked wc_no_products_found - 10
							 */
							do_action( 'woocommerce_no_products_found' );
							?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer( 'shop' ); ?>
