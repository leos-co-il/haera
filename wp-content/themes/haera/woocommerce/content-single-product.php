<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$post_link = get_the_permalink();
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$id = $product->get_id();
$title_1 = lang_text(['he' => 'מידע נוסף', 'en' => 'More info'], 'he');
$title_2 = lang_text(['he' => 'משלוחים והחזרות', 'en' => 'Shipments and returns'], 'he');
$accordion_info = [
		['title' => $title_1, 'content' => get_field('product_more_info', $id)],
		['title' => $title_2 , 'content' => get_field('product_delivery_info', $id)],
];
?>
<div id="product-<?php the_ID(); ?>" class="container-fluid">
	<?php
	echo '<div class="row justify-content-center">
<div class="col-xl-10 col-11">';
	the_title('<h1 class="block-title product-page-title product-page-title-hide-big">', '</h1>');
	echo '<div class="row flex-row-reverse justify-content-between align-items-start">';
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>
	<div class="col-lg-5 col-12 product-main-col">
		<?php if ( ! defined( 'ABSPATH' ) ) {
			exit; // Exit if accessed directly.
		}
		the_title( '<h1 class="block-title product-page-title product-page-title-hide">', '</h1>' ); ?>
		<div class="product-meta">
			<?php

			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );

			?>
		</div>
		<?php if ($accordion_info) : ?>
			<div id="accordion-product" class="product-info-acc">
				<?php foreach ($accordion_info as $number => $info_item) : ?>
					<div class="card">
						<div class="prod-desc-header" id="heading_<?= $number; ?>">
							<div class="accordion-button-wrap">
								<h2 class="product-info-title"><?= $info_item['title']; ?></h2>
								<button class="btn" data-toggle="collapse"
										data-target="#contactInfo<?= $number; ?>"
										aria-expanded="false" aria-controls="collapseOne">
															<span class="accordion-symbol plus-icon
															<?php echo $number === 0 ? 'hide-icon' : ''; ?>">+</span>
									<span class="accordion-symbol minus-icon
															<?php echo $number === 0 ? 'show-icon' : ''; ?>">-</span>
								</button>
							</div>
							<div id="contactInfo<?= $number; ?>" class="collapse
											<?php echo $number === 0 ? 'show' : ''; ?>"
								 aria-labelledby="heading_<?= $number; ?>" data-parent="#accordion-product">
								<div class="slider-output">
									<?= $info_item['content']; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<div class="socials-share">
					<span class="share-text">
						<?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
			<!--	WHATSAPP-->
			<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
				<img src="<?= ICONS ?>whatsapp-share.png">
			</a>
			<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
			   class="social-share-link">
				<img src="<?= ICONS ?>facebook-share.png">
			</a>
			<!--	MAIL-->
			<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
			   class="social-share-link">
				<img src="<?= ICONS ?>mail-share.png">
			</a>
		</div>
	</div>
	<?php
	echo '</div></div></div>';
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
