<?php

$tel = opt('tel');
$mail = opt('mail');
$instagram = opt('instagram');
$whatsapp = opt('whatsapp');
$facebook = opt('facebook');
$address = opt('address');
$open_hours = opt('open_hours');
$menus_arr = [
		'menu_0' => 'footer-main-menu',
		'menu_1' => 'footer-second-menu',
		'menu_2' => 'footer-third-menu',
		'menu_3' => 'footer-fourth-menu',
];
$menusTitles = [];
if ($menu_titles = opt('menu_titles')) {
	foreach ($menu_titles as $x => $title) {
		$menusTitles[]['title'] = isset($title['foo_menu_title']) ? $title['foo_menu_title'] : '';
	}
}
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
?>


<footer>
	<div class="footer-main">
		<?php if ($current_id !== $contact_id) : ?>
			<div class="footer-form">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="row justify-content-center">
								<?php if ($logo_form = opt('logo')) : ?>
									<div class="col-lg-5 col-sm-7 col-9 mb-3">
										<img src="<?= $logo_form['url']; ?>" alt="logo">
									</div>
								<?php endif; ?>
								<div class="col-12">
									<?php if ($foo_f_title = opt('foo_form_title')) : ?>
										<div class="foo-form-text"><?= $foo_f_title; ?></div>
									<?php endif;
									lang_form(['he' => '13', 'en' => '93'], '13') ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container foo-links-container">
				<div class="row justify-content-center">
					<?php if ($address) : ?>
						<a class="col-xl col-md-4 col-6 foo-link" href="https://waze.com/ul?q=<?= $address; ?>" target="_blank">
							<span class="foo-contact-icon">
								<?= svg_simple(ICONS.'foo-geo.svg'); ?>
							</span>
							<h2 class="foo-link-title">
								<?php
								$fax_text = ['he' => 'אנחנו נמצאים כאן', 'en' => 'We are here'];
								echo lang_text($fax_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle"><?= $address; ?></p>
						</a>
					<?php endif; ?>
					<?php if ($instagram) : ?>
						<a href="<?= $instagram; ?>" class="col-xl col-md-4 col-6 foo-link" target="_blank">
								<span class="foo-contact-icon">
									<img src="<?= ICONS ?>foo-instagram.png">
								</span>
							<h2 class="foo-link-title">
								<?php
								$insta_text = ['he' => 'עקבו אחרנו', 'en' => 'Follow us'];
								echo lang_text($insta_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle">
								<?php
								$insta_text_2 = ['he' => 'האינסטגרם שלנו בלחיצה', 'en' => 'Our instagram at a click'];
								echo lang_text($insta_text_2, 'he');
								?>
							</p>
						</a>
					<?php endif; ?>
					<?php if ($facebook) : ?>
						<a href="<?= $facebook; ?>" class="col-xl col-md-4 col-6 foo-link" target="_blank">
							<span class="foo-contact-icon">
								<img src="<?= ICONS ?>foo-facebook.png">
							</span>
							<h2 class="foo-link-title">
								<?php
								$facebook_text = ['he' => 'עשו לנו לייק', 'en' => 'Like us'];
								echo lang_text($facebook_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle">
								<?php
								$facebook_text_2 = ['he' => 'בואו להיות חברים שלנו', 'en' => "Let's be our friends"];
								echo lang_text($facebook_text_2, 'he');
								?>
							</p>
						</a>
					<?php endif; ?>
					<?php if ($whatsapp) : ?>
						<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="col-xl col-md-4 col-6 foo-link"
						   target="_blank">
								<span class="foo-contact-icon">
									<img src="<?= ICONS ?>foo-whatsapp.png">
								</span>
							<h2 class="foo-link-title">
								<?php
								$whatsapp_text = ['he' => 'שלחו לנו הודעה', 'en' => 'Send us a message'];
								echo lang_text($whatsapp_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle">
								<?php
								$whatsapp_text_2 = ['he' => 'אז מה נשמע?', 'en' => "So what's up?"];
								echo lang_text($whatsapp_text_2, 'he');
								?>
							</p>
						</a>
					<?php endif; ?>
					<?php if ($tel) : ?>
						<a href="tel:<?= $tel; ?>" class="col-xl col-md-4 col-6 foo-link">
								<span class="foo-contact-icon">
									<?= svg_simple(ICONS.'foo-tel.svg'); ?>
								</span>
							<h2 class="foo-link-title">
								<?php
								$tel_text = ['he' => 'התקשרו אלינו', 'en' => 'Call us'];
								echo lang_text($tel_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle">
								<?= $open_hours ? $open_hours : $tel; ?>
							</p>
						</a>
					<?php endif; ?>
					<?php if ($mail) : ?>
						<a href="mailto:<?= $mail; ?>" class="foo-link col-xl col-md-4 col-6">
								<span class="foo-contact-icon">
									<?= svg_simple(ICONS.'foo-mail.svg'); ?>
								</span>
							<h2 class="foo-link-title">
								<?php
								$mail_text = ['he' => 'שלחו לנו מייל', 'en' => 'Send us an email'];
								echo lang_text($mail_text, 'he');
								?>
							</h2>
							<p class="foo-link-subtitle"><?= $mail; ?></p>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<a id="go-top">
			<span class="top-arrow-wrap">
				<img src="<?= ICONS ?>to-top.svg">
			</span>
			<h5 class="top-button-text">
				<?= lang_text(['he' => 'חזרה למעלה', 'en' => 'To top'], 'he'); ?>
			</h5>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-center mt-3">
				<?php foreach ($menusTitles as $k => $menu) : ?>
					<div class="col-lg col-6 foo-menu">
						<?php if ($menu['title']) : ?>
							<h3 class="foo-title"><?= $menu['title']; ?></h3>
						<?php endif; ?>
						<div class="menu-border-top">
							<?php
							$index = 'menu_'.$k;
							getMenu($menus_arr[$index], '1'); ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
