<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body mt-4 mb-5 camp-body">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-md-11 col-12">
				<div class="row justify-content-center">
					<?php if ( function_exists('yoast_breadcrumb') ) : ?>
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-between align-items-start">
					<div class="col-lg-7 col-12 post-content-col">
						<h1 class="base-title"><?php the_title(); ?></h1>
						<div class="base-output base-single-output">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-lg-5 col-12 camp-img-col">
						<img src="<?= IMG ?>emblem.png" alt="emblem" class="emblem">
						<?php if (has_post_thumbnail()) : ?>
							<img src="<?= postThumb(); ?>" alt="camp-img">
						<?php endif;
						if ($fields['camp_images']) : foreach ($fields['camp_images'] as $img) : ?>
							<img src="<?= $img['url']; ?>" alt="camp-img">
						<?php endforeach; endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<section class="form-camp-block">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<?php lang_form(['he' => '633', 'en' => '634'], '633'); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
