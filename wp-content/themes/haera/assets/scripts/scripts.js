(function($) {
	var x = 12;
	$('.gallery-row .col-gal:lt('+x+')').addClass('show');
	$('.load-more-img').click(function () {
		var parentId = $(this).data('id');
		var parentNow = document.getElementById(parentId);
		var sizeLi = $(parentNow).children('.col-gal').size();
		x = (x + 4 <= sizeLi) ? x + 4 : sizeLi;
		$(parentNow).children('.col-gal:lt('+x+')').addClass('show');
		if (x === sizeLi) {
			$(this).addClass('hide');
		}
	});
	$( document ).ready(function() {
		$(document.body).on('click', '.get-vacancy' , function(){
			var vac = $(this).data('vacancy');
			$('#vacancy-hidden').val(vac);
			var form = $('.take-form').html();
			$($(this).data('target')+' .put-form-here').html(form);
			$($(this).data('target')).addClass('show-vacancy');
			$('.vac-page-pop').addClass('show-popup');
		});
		$(document.body).on('click', '.close-form' , function(){
			$($(this).data('target')).removeClass('show-vacancy');
			$('.vac-page-pop').removeClass('show-popup');
		});
		$('.counter-number').rCounter({
			duration: 30
		});
		//Input file
		$('#foo-vacancy-file').on('input',function(){
			if($(this).val().length>0){
				$(this).addClass('full');
				$('.file-name').append($(this).val());
			}
			else{
				$(this).removeClass('full');
			}
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.trigger-wrap').hover(function(){ // задаем функцию при наведении курсора на элемент
			$(this).children('.all-socials').addClass('show-share');
		}, function(){ // задаем функцию, которая срабатывает, когда указатель выходит из элемента
			$(this).children('.all-socials').removeClass('show-share');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').toggleClass('show-popup');
			$('.float-form').toggleClass('show-float-form');
		});
		$('.pop-close').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});
		var accordionCats = $('#accordion-cats');
		accordionCats.collapse({
			toggle: false
		});
		$('.main-video-wrapper svg').on('click',function(){
			$(this).css('display', 'none');
		});
		$('.accordion-trigger').on('click',function(){
			$(this).toggleClass('minus-acc');
			$(this).parent('.card-header').parent('.card').children('.body-acc').toggle('show');
			$(this).parent('.card-header').toggleClass('active-card');
		});
		var accordionProduct = $('#accordion-product');
		accordionProduct.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').removeClass('show-icon');
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').removeClass('hide-icon');
		});
		accordionProduct.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').addClass('show-icon');
			show.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').addClass('hide-icon');
		});
		$('.slider-vacancies').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.news-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			vertical: true,
			arrows: true,
			verticalSwiping: true,
			dots: false,
			centerPadding: 0,
		});
		$('.events-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			vertical: true,
			centerMode: false,
			verticalSwiping: true,
			centerPadding: 0,
			arrows: true,
			dots: false,
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.prod-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.add-to-cart-btn').click(function (e) {
			e.preventDefault();

			var btn = $(this);
			var productId = $(this).data('id');
			var quantity = 1;
			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: productId,
					quantity: quantity,
				},

				success: function (results) {
					btn.children('.check-icon').addClass('add-success');
					btn.children('.not-added-yet').addClass('add-success');
				}
			});
		});
	});
	//More posts
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' ),
		paged = button.data( 'paged' ),
		maxPages = button.data( 'max_pages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault(); // предотвращаем клик по ссылке
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function( xhr ) {
				button.text(textLoad);
			},
			success : function( data ){

				paged++; // инкремент номера страницы
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				// если последняя страница, то удаляем кнопку
				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}

			}

		});

	} );
	$(document).on('click', '.plus, .minus', function() {

		var qty = $(this).parent('.woocommerce-variation-add-to-cart').children('.quantity').children('input');
		var val = parseFloat(qty.val());
		var max = 9999;
		var min = 1;
		var step = 1;

		if ($(this).is('.plus')) {
			if (max && (max <= val)) {
				qty.val(max);
			} else {
				qty.val(val + step);
			}
		} else {
			if (min && (min >= val)) {
				qty.val(min);
			} else if (val > 1) {
				qty.val(val - step);
			}
		}

	});
})( jQuery );
