<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$logo = opt('logo');
?>


<header>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-between">
				<div class="col-lg-2 hidden-head-col"></div>
				<div class="col">
					<nav id="MainNav" class="h-100">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
					</nav>
				</div>
				<div class="col-auto">
					<div class="reg-lang-wrap">
						<?php site_languages(); ?>
						<a href="<?= wp_registration_url(); ?>" class="register-link">
							<span>
								<?= lang_text(['he' => 'התחבר | הרשם', 'en' => 'Login | Sign up'], 'he'); ?>
							</span>
							<img src="<?= ICONS ?>user.png">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container-fluid">
			<div class="row row-header-bottom">
				<?php if ($logo = opt('logo_dark')) : ?>
					<div class="col-lg-2 col-auto d-flex justify-content-end align-items-center">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
				<div class="col-md col-6 max-col d-flex align-items-center">
					<nav class="bottom-nav">
						<?php getMenu('header-bottom-menu', '2'); ?>
					</nav>
				</div>
				<?php if ($tel = opt('tel')) : ?>
					<div class="col-auto">
						<a href="tel:<?= $tel; ?>" class="header-tel">
							<span class="tel-wrap-small">
								<span class="tel-number">
									<?= lang_text(['he' => 'התקשרו', 'en' => 'Call'], 'he'); ?>
								</span>
							<span class="mx-2"><?= $tel; ?></span>
							</span>
							<span class="tel-text">
								<?= lang_text(['he' => 'אנחנו כאן מחכים לכם', 'en' => 'We are here waiting for you'], 'he'); ?>
							</span>
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>

<?php
$whatsapp = opt('whatsapp');
$instagram = opt('instagram');
$facebook = opt('facebook');
$youtube = opt('youtube');
$socials = get_social_links(['whatsapp', 'facebook', 'youtube', 'instagram']);
if ($socials) : ?>
	<div class="fixed-socials">
		<?php foreach ($socials as $fix_social) : ?>
			<a href="<?= $fix_social['url']; ?>" class="fix-social-link">
				<?= svg_simple(ICONS.$fix_social['name'].'.svg'); ?>
			</a>
		<?php endforeach; ?>
		<div class="fix-social-link pop-trigger">
			<img src="<?= ICONS ?>mail.png">
		</div>
	</div>
<?php endif; ?>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="pop-form-col">
						<?php if ($logo_pop = opt('logo')) : ?>
							<a href="/" class="logo">
								<img src="<?= $logo_pop['url'] ?>" alt="logo">
							</a>
						<?php endif;
						if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('pop_form_subtitle')) : ?>
							<h3 class="pop-form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						if ($f_text = opt('pop_form_text')) : ?>
							<p class="base-text mb-3"><?= $f_text; ?></p>
						<?php endif;
						lang_form(['he' => '12', 'en' => '92'], '12') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="fix-wrap-col">
	<div class="fix-wrap-item">
		<img src="<?= ICONS ?>search.png" alt="search" class="inside-circle-img">
	</div>
	<a class="fix-wrap-item" href="<?= wc_get_cart_url(); ?>">
		<?= svg_simple(ICONS.'basket.svg'); ?>
	</a>
</div>
